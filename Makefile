# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: dspyrydo <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/06/15 20:13:38 by dspyrydo          #+#    #+#              #
#    Updated: 2017/06/15 20:13:50 by dspyrydo         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libftprintf.a
FLAGS = -Wextra -Wall -Werror
INCLUDES = -I . -I ./libft
OBJECTS = 							    \
			ft_printf.o 				\
			flags_1.o 					\
			ft_itoa_base.o 				\
			flags_2.o 					\
			functions_2.o 				\
			functions_1.o 				\
			modifiers.o 				\
			types_1.o 					\
			types_2.o 					\
			functions_3.o 				\
			functions_4.o 				\
			functions_5.o               \
			functions_p.o 				\
			ft_putchar_pf.o             \
			ft_putstr_pf.o              \
			wchar_functions.o           \
			ft_countdigits.o 			\
			ft_strchr.o 				\
			ft_isalpha.o				\
			ft_isdigit.o				\
			ft_strlen.o					\
			ft_isalnum.o 				\
			ft_strdup.o         		\
			ft_strcpy.o         		\
			ft_strnew.o         		\
			ft_bzero.o          		\
			ft_strdel.o         		\
			ft_strcmp.o         		\

all : $(NAME)
$(NAME) : $(OBJECTS)
	ar rc $(NAME) $(OBJECTS)
	ranlib $(NAME)
%.o : %.c
	gcc $(FLAGS) -o $@ -c $< $(INCLUDES)
clean :
	rm -f $(OBJECTS)
	rm -f *.c~
fclean : clean
	rm -f $(NAME)
re : fclean all
